import { useEffect, useState } from "react"

function MyComponent(){

    const [width,setWidth] = useState(window.innerWidth)
    const [height,setHeight] = useState(window.innerHeight)

    const handleResize = () => {
        setWidth(window.innerWidth)
        setHeight(window.innerHeight)
    }
   
    // 添加windows的监听事件只执行一次
    useEffect(()=>{
        window.addEventListener("resize",handleResize)
        console.log("resize add");

        // 清理工作,组件卸载的时候
        return ()=>{
            window.removeEventListener("resize",handleResize)
        }
    },[])


    return (
        <>
            <p>Window Width: {width} px</p>
            <p>Window Height: {height} px</p>
        </>
    )
}

export default MyComponent