// 默认会寻找index文件
import Button from "./components/Button";

function App() {
  return (
    // children的传递在标签中
    <Button>
      hello <span style={{ color: "red" }}>World</span>
    </Button>
  );
}

export default App;
