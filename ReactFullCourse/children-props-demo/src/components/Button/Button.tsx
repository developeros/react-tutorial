import { ReactNode } from "react";
// 固定类型
interface ButtonProps {
  onClick?: () => void; // ? 可选属性
  children: ReactNode; 
}
// 设置默认值
function Button({
  onClick = () => console.log("hello"),
  children,
}: ButtonProps) {
  return <button onClick={onClick}>{children}</button>;
}

export default Button;
