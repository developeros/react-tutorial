import { useState } from "react";

function MyForm() {
  const [name, setName] = useState("游客");
  const [quantity, setQuantity] = useState(0);
  const [comments, setComments] = useState("");
  const [payment, setPayment] = useState("");
  const [shipping, setShipping] = useState("Delivery");

  const handleNameChange = (e) => setName(e.target.value);
  const handleQuantityChange = (e) => setQuantity(e.target.value);
  const handleCommentsChange = (e) => setComments(e.target.value);
  const handlePaymentChange = (e) => setPayment(e.target.value);
  const handleShippingChange = (e) => setShipping(e.target.value);

  return (
    <div className="form-container">
      {/* input text */}
      <input type="text" value={name} onChange={handleNameChange} />
      <p>Name: &nbsp; {name}</p>
      <hr/>
      {/* input number */}
      <input type="number" value={quantity} onChange={handleQuantityChange} />
      <p>Quantity: &nbsp; {quantity}</p>
      <hr/>
      {/* textarea */}
      <textarea
        value={comments}
        onChange={handleCommentsChange}
        placeholder="Delivery Instructions"
      />
      <p>Comments: &nbsp; {comments}</p>
      <hr/>
      {/* select */}
      <select value={payment} onChange={handlePaymentChange}>
        <option value="">选择支付方式</option>
        <option value="微信支付">微信支付</option>
        <option value="支付宝">支付宝</option>
      </select>
      <p>Payment: &nbsp; {payment}</p>
      <hr/>
      {/* radio */}
      <label>
        <input
          type="radio"
          value="Pick Up"
          checked={shipping === "Pick Up"}
          onChange={handleShippingChange}
        />
        Pick Up
      </label>
      <br />

      <label>
        <input
          type="radio"
          value="Delivery"
          checked={shipping === "Delivery"}
          onChange={handleShippingChange}
        />
        Delivery
      </label>
      <p>Shipping: &nbsp; {shipping}</p>
    </div>
  );
}

export default MyForm;
