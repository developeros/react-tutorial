export default function Footer(){
    return (
        <footer>
            {/* 可以在这里写js代码 */}
            <p>&copy; {new Date().getFullYear()} huangzhuangzhuang</p>
        </footer>
    )
}