import List from "./List.jsx"

function App() {

    const fruits = [
        {id: 1, name: "apple",calories: 95},
        {id: 2, name: "orange",calories: 55},
        {id: 3, name: "banana",calories: 105},
        {id: 4, name: "coconut(椰子)",calories: 159},
        {id: 5, name: "pineapple(菠萝)",calories: 37},
    ]

    const vegetables = [
        {id: 6, name: "potatoes(土豆)",calories: 110},    
        {id: 7, name: "celery(芹菜)",calories: 15},   
        {id: 8, name: "carrots(葫芦卜)",calories: 25},   
        {id: 9, name: "corn(玉米)",calories: 63}, 
        {id: 10, name: "broccoli(西兰花)",calories: 50},
    ]

    return (
        <>
            <List category="Fruits" items={fruits}></List>
            <List category="Vegetables" items={vegetables}></List>
        </>
    )
}

export default App
