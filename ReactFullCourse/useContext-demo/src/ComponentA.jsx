import { createContext } from "react";
import ComponentB from "./ComponentB";
import { useState } from "react";

export const UserContext = createContext();

export default function ComponentA() {
  const [user, setUser] = useState("胖卡");
  return (
    <div className="box">
      <h1>Component A</h1>
      <p>Hello {user}</p>
      <UserContext.Provider value={user}>
        <ComponentB />
      </UserContext.Provider>
    </div>
  );
}
