import "./index.css"
import Student from "./Student.jsx"

function App(){
    return(
        <>
            <Student name="卡卡" age={18} isStudent={true} />
            <Student name="曾国藩" age={60} />
            <Student />
        </>
    )
}

export default App