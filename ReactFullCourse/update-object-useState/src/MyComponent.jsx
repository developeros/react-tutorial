import { useState } from "react";

export default function MyComponent() {
  const [car, setCar] = useState({
    year: 2024,
    maker: "小米",
    model: "XiaoMI SU7",
  });

  const handleCarYearChange = e => setCar(c => ({...c,year:e.target.value}))
  const handleCarMakerChange = e => setCar(c => ({...c,maker:e.target.value}))
  const handleCarModelChange = e => setCar(c => ({...c,model:e.target.value}))

  return (
    <>
        <p>Your favorite Car is: {car.year} {car.maker} {car.model}</p>
        <input type="number" value={car.year} onChange={handleCarYearChange}/><br/>
        <input type="text" value={car.maker} onChange={handleCarMakerChange} /><br/>
        <input type="text" value={car.model} onChange={handleCarModelChange}/><br/>
    </>
  )
}
