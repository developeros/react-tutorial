
interface SquareProps{
  value: string;
  onSquareClick: (event: React.MouseEvent<HTMLButtonElement>) => void
}

export default function Square({value,onSquareClick}: SquareProps) {

  return (
    <button onClick={onSquareClick} className="square">
      {value}
    </button>
  );
}
