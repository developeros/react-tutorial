import { useState } from "react"

export default function ColorPicker(){

    const [color,setColor] = useState("#ffffff")

    const handleColorChange = e => setColor(e.target.value)

    return (
        <div className="color-picker__container">
            <h1>Color Picker</h1>
            <div className="color-display" style={{backgroundColor: color}}>
                <p>Select Color {color}</p>
            </div>
            <label htmlFor="choose-color">Select a Color</label>
            <input id="choose-color" type="color" value={color} onChange={handleColorChange}/>
        </div>
    )
}