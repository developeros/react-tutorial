import { useState } from "react"

function Counter(){
    const [count,setCount] = useState(0)

    // 两种函数写法都可以，箭头函数更加方便
    function decrement(){
        // setCount(count - 1)
        setCount(c => c - 1)
    }

    const increment = () => setCount(c => c + 1)   //setCount(count + 1)

    const reset = () => setCount(0)

    return (
        <div className="counter-container">
            <p className="counter-display">{count}</p>
            <button className="counter-btn" onClick={decrement}>Decrement</button>
            <button className="counter-btn" onClick={reset}>Reset</button>
            <button className="counter-btn" onClick={increment}>Increment</button>
        </div>
    )
}

export default Counter