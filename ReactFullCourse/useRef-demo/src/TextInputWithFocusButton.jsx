import { useRef } from "react";

function TextInputWithFocusButton() {
  const inputEl = useRef(null);
  return (
    <>
      {/* 挂载 */}
      <input type="text" ref={inputEl} />
      {/* `current` 指向已挂载到 DOM 上的 text 输入元素 */}
      <button onClick={() => inputEl.current.focus()}>Focus the input</button>
    </>
  );
}

export default TextInputWithFocusButton;
