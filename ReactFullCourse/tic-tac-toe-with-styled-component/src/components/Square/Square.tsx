import styled from "styled-components";

const SquareButton = styled.button`
  margin-top: -1px;
  margin-left: -1px;
  font-size: 24px;
  background-color: white;
  border: 1px solid #999;
`

interface SquareProps{
  value: string;
  onSquareClick: (event: React.MouseEvent<HTMLButtonElement>) => void
}

export default function Square({value,onSquareClick}: SquareProps) {

  return (
    <SquareButton onClick={onSquareClick} className="square">
      {value}
    </SquareButton>
  );
}
