function Button() {
    const handleClick = (event, param1, param2) => {
        console.log('Clicked!', param1, param2);
        // You can access the event object here if needed
        console.log(event.target.textContent);
      };
    
      return (
        <button onClick={(event) => handleClick(event, 'param1', 'param2')}>
          Click me😘
        </button>
      );
}

export default Button;
