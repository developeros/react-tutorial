import axios from "axios";
import { useEffect, useState } from "react";

// api会返回这个对象很对属性，我们只约束我们感兴趣的属性
interface User {
  id: number;
  name: string;
}

function App() {
  const[users,setUsers] = useState<User[]>([])

  // 获取数据
  const fetchData = async()=>{
    let response = await axios.get<User[]>(
      "https://jsonplaceholder.typicode.com/users"
    );
    setUsers(response.data)
  }

  // 在组件挂在时获取数据
  useEffect(() => {
    fetchData()
  }, []);

  return (
    <ul>
      {users.map(user => <li key={user.id}>{user.name}</li>)}
    </ul>
  )
}

export default App;
