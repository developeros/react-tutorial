import profilePic from "./assets/profile.jpg"

function Card(){
    return (
        <div className="card-container">
            <img src={profilePic} alt="" className="card-image" />
            <h2 className="card-title">胖卡</h2>
            <p className="card-text">Hello,胖卡Learning React</p>
        </div>
    )
}

export default Card