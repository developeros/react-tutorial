import PropTypes from "prop-types";

function UserGreeting(props) {
    // 把组件单独抽离出来
  const welcomeMessage = (
    <h2 className="welcome-message">Welcome {props.name}</h2>
  );

  const loginPrompt = <h2 className="login-prompt">Please log to continue</h2>;

  return props.isLoggedIn ? welcomeMessage : loginPrompt;
}

UserGreeting.propTypes = {
  isLoggedIn: PropTypes.bool,
  name: PropTypes.string,
};

UserGreeting.defaultProps = {
  isLoggedIn: false,
  name: "游客",
};

export default UserGreeting;
