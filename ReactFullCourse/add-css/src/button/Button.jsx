// import styles from "./button.module.css"


function Button(){

    const style = {
        color: "white",
        backgroundColor: "rgb(67, 205, 223)",
        padding: "20px",
        fontSize: "16px",
        border: "none",
        borderRadius: "10px",
        cursor: "pointer",
    }

    return (
        <button style={style}>Click Me</button>
    )
}

export default Button