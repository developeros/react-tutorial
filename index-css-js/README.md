

> 使用vite启动部署

|  MOC   |   访问  |
| --- | --- |
|  原生JS   |  [http://localhost:5173/vanilla-js-html/](./vanilla-js-html/)   |
|  JQuery   |  [http://localhost:5173/jquery-js-html/](./jquery-js-html/)   |
|  React   |  [http://localhost:5173/react-js-html/](./react-js-html/)   |
|  React simple component   |  [http://localhost:5173/simple-component-in-react/](./simple-component-in-react/)   |
|  component with props in react   |  [http://localhost:5173/component-with-props-in-react/](./component-with-props-in-react/)   |
|  state in component   |  [http://localhost:5173/state-in-component/](./state-in-component/)   |
|  esm in React   |  [http://localhost:5173/esm-in-react/](./esm-in-react/)   |
