import LikeButton from "./like-button"

 
function Header({ title }) {
  return <h1>{title ? title : 'Default title'}</h1>;
}
 
function HomePage() {
  const languages = ['JavaScript', 'TypeScript', 'Java'];
 
  return (
    <div>
      <Header title="Pkmer Learning React with Next.js" />
      <ul>
        {languages.map((name) => (
          <li key={name}>{name}</li>
        ))}
      </ul>
 
      <LikeButton />
    </div>
  );
}

/**
 * Because this file contains two components: HomePage and Header 
 * help Next.js distinguish which component to render
 *  as the main component of the page.
 */
export default HomePage;