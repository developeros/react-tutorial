/**
 * 没有导入React会报错
 * Uncaught ReferenceError: React is not defined
    at test.jsx:18:5
 */
import React from 'react';

function introduce(user) {
    return user.name + ' ' + user.desc;
  }
  
  const user = {
    name: '胖卡',
    desc: 'Love coding with React🤗'
  };
  
  const element = (
    <h1>
      Hello, {introduce(user)}!
    </h1>
  );

  export default element;