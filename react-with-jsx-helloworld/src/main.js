import './style.css'
import { createRoot } from 'react-dom/client';
import element from './test'


const root = createRoot(document.getElementById('app'));

root.render(element);