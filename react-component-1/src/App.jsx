import './App.css'

function introduce(user){
  return user.name + ' ' + user.desc;
}

function App() {
  const user = {
    name: '胖卡',
    desc: 'Love coding with React🤗'
  };

  return (
    <h1>
      Hello, {introduce(user)}!
    </h1>
  )
}

export default App
