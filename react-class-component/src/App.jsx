import React from 'react';
import './App.css'

export default class App extends React.Component{
  constructor(){
    super();
    this.state = {
      user: {
        name: '胖卡',
        desc: 'Love coding with React🤗'
      }
    }
  }

  introduce = () =>{
    return this.state.user.name + ' ' + this.state.user.desc;
  }

  render(){
    return (
      <h1>
        Hello, {this.introduce()}!
      </h1>
    );
  }
}