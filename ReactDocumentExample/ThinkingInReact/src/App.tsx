import PRODUCTS from "@/data/product"
import FilterTableProductTable from "@/components/FilterTableProductTable"

const App = () => <FilterTableProductTable products={PRODUCTS}/>
export default App