import SearchBar from "@/components/SearchBar";
import ProductTable from "@/components/ProductTable";
import { useState } from "react";
import type { Product } from "@/data/product";

interface Props{
  products: Product[]
}

function FilterTableProductTable({products}:Props) {
  const [filterText, setFilterText] = useState("");
  const [inStockOnly, setInStockOnly] = useState(false);

  return (
    <>
      <SearchBar
        filterText={filterText}
        inStockOnly={inStockOnly}
        onFilterTextChange={setFilterText}
        onInStockOnlyChange={setInStockOnly}
      />
      <ProductTable
        filterText={filterText}
        inStockOnly={inStockOnly}
        products={products}
      />
    </>
  );
}

export default FilterTableProductTable;
