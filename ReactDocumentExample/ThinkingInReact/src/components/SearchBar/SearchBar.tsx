interface SearchBarProps {
  filterText: string;
  inStockOnly: boolean;
  onFilterTextChange: (value: string) => void;
  onInStockOnlyChange: (value: boolean) => void;
}

function SearchBar(props: SearchBarProps) {
  return (
    <form className="search-bar">
      <input
        type="text"
        value={props.filterText}
        placeholder="Search..."
        onChange={(e) => props.onFilterTextChange(e.target.value)}
      />
      <div>
        <label>
          <input
            type="checkbox"
            checked={props.inStockOnly}
            onChange={(e) => props.onInStockOnlyChange(e.target.checked)}
          />{" "}
          是否只展示有库存的商品
        </label>
      </div>
    </form>
  );
}

export default SearchBar;
