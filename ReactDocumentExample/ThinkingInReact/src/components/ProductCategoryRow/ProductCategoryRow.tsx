function ProductCategoryRow({ categoryName = "Default Category" }) {
  return (
    <tr>
      <th colSpan={2}>{categoryName}</th>
    </tr>
  );
}

export default ProductCategoryRow;
