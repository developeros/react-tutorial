import type { Product } from "@/data/product"

// const ProductRow:React.FC<Product> = (product) => {
//   return (
//     <div>{product.name}</div>
//   )
// }

interface ProductRowProps {
  product: Product;
}

function ProductRow({ product }: ProductRowProps) {
  const name = product.stocked ? (
    product.name
  ) : (
    <span style={{ color: "red" }}>{product.name}</span>
  );


  return (
    <tr>
      <td>{name}</td>
      <td>{product.price}</td>
    </tr>
  );
}

export default ProductRow;
