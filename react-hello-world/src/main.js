import './style.css'
import React from 'react';
import { createRoot } from 'react-dom/client';

const root = createRoot(document.getElementById('app'));

root.render(<h1>Hello, world</h1>);

//root.render(<video class="styles_img3__396Xj" src="https://framerusercontent.com/assets/FE0IPswHicoB30Vdpion8dfYg.mp4" autoplay="true" loop="true" playsinline="" preload="true"></video>)
